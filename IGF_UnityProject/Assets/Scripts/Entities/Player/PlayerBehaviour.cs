﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MovableEntity
{
    private bool m_Jump = false;
    private bool m_Interact = false;
    private bool m_Sprint = false;
    private bool m_IsGrounded = false;

    private Vector3 m_GroundNormal;
    private float m_GroundRaycastMagnitude = 1.2f;

    protected override void Start()
    {
        m_MovementSettings = new MovementSettings(10, 2.8f, 2, 1.6f, 0.5f, 0.8f);
        base.Start();
    }

    protected override void FixedUpdate()
    {
        CheckGround();
        MovementLogic();
    }

    private void MovementLogic()
    {
        m_MovementEngine.Begin();

        //Rotation
        if (m_Rotation != Vector3.zero)
            m_MovementEngine.Rotate(m_Rotation);

        //Slanted surfaces
        if(m_IsGrounded)
            m_MovementEngine.ApplyMovement(m_MovementDirection, m_Sprint, m_GroundNormal);
        else
            m_MovementEngine.ApplyMovement(m_MovementDirection, m_Sprint);

        //Jump
        if (m_Jump && m_IsGrounded)
        {
            m_MovementEngine.InitJump();
            m_Jump = false;
        }

        m_MovementEngine.End();
    }

    private void CheckGround()
    {
        RaycastHit hit;

        if(EnvironmentInfo.RaycastToGround(transform.position + m_CharacterController.center, out hit, m_GroundRaycastMagnitude))
        {
            if(!m_IsGrounded)
                Debug.Log("Landed");


            m_GroundNormal = hit.normal;
            m_IsGrounded = true;
            
        }
        else
        {
            if (m_IsGrounded)
                Debug.Log("Airborn");

            m_IsGrounded = false;
           
        }      

    }

    public void Jump()
    {
        if(m_IsGrounded)
            m_Jump = true;
    }

    public void ToggleSprint()
    {
        m_Sprint = !m_Sprint;
    }

    public void Interact()
    {
        m_Interact = true;
    }

}
