﻿using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField]
    private PlayerBehaviour player;

    [SerializeField]
    private Camera playerCamera;

    private InputHandler m_InputHandler;

    public void Start()
    {
        m_InputHandler = new InputHandler();
        m_InputHandler.Register(InputRegistration.Create("Jump", new JumpCommand(player)));
        m_InputHandler.Register(InputRegistration.Create("Interact", new InteractCommand(player)));
        m_InputHandler.Register(InputRegistration.Create("LeftStick_Horizontal", "LeftStick_Vertical", new RotateCommand(player, playerCamera)));
        m_InputHandler.Register(InputRegistration.Create("LeftStick_Horizontal", "LeftStick_Vertical", new MoveCommand(player, playerCamera)));

    }

    public void Update()
    {
        m_InputHandler.Update();
    }
}