﻿using UnityEngine;

public class MovableEntity : MonoBehaviour
{
    protected MovementSettings m_MovementSettings;

    protected CharacterController m_CharacterController;
    protected MovementEngine m_MovementEngine;

    protected Vector3 m_Rotation;
    protected Vector3 m_MovementDirection;

    protected virtual void Start()
    {
        m_CharacterController = GetComponent<CharacterController>();
        m_MovementEngine = new MovementEngine(m_MovementSettings, m_CharacterController);
    }

    protected virtual void FixedUpdate()
    { }

    public virtual void Move(Vector3 direction)
    {
        m_MovementDirection = direction;
    }

    public virtual void Rotate(Vector3 direction)
    {
        
        m_Rotation = direction;
    }
}