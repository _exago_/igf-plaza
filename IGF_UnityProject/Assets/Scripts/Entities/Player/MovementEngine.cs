﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementSettings
{
    private readonly float m_speed;
    public float Speed { get { return m_speed; } }

    private readonly float m_jumpHeight;
    public float JumpHeight { get { return m_jumpHeight; } }

    private readonly float m_sprintMultiplier;
    public float SprintMultiplier { get { return m_sprintMultiplier; } }

    private readonly float m_jumpBoostMultiplier;
    public float JumpBoostMultiplier { get { return m_jumpBoostMultiplier; } }

    private readonly float m_deltaTimeToGetTopSpeed;
    public float DeltaTimeToGetTopSpeed { get { return m_deltaTimeToGetTopSpeed; } }

    private readonly float m_airDrag;
    public float AirDrag { get { return m_airDrag; } }

    public MovementSettings(float speed, float jumpheight, float sprintMultiplier, float jumpBoostMultiplier, float deltaTimeToGetTopSpeed, float airDrag)
    {
        m_speed = speed;
        m_jumpHeight = jumpheight;
        m_sprintMultiplier = sprintMultiplier;
        m_jumpBoostMultiplier = jumpBoostMultiplier;
        m_deltaTimeToGetTopSpeed = deltaTimeToGetTopSpeed;
        m_airDrag = airDrag;
    }
}

public class MovementEngine
{
    private MovementSettings m_Settings;
    private CharacterController m_CharacterController;

    private const float SLOPEFORCE = 5;

    private Vector3 m_Velocity;
    private Vector3 m_PreviousDirection;
    private float m_acceleration;
    private bool m_Stopped = true;

    private float m_jumpAccel = 0;

    public MovementEngine(MovementSettings movementSettings, CharacterController characterController)
    {
        m_Settings = movementSettings;
        m_CharacterController = characterController;
    }

    public void Begin()
    {
        m_Velocity = m_CharacterController.velocity;
        m_Velocity = Vector3.Scale(m_Velocity, new Vector3(0, 1, 0));
        ApplyGravity();
    }

    private void ApplyGravity()
    {
        m_Velocity += Physics.gravity * Time.deltaTime;
    }

    public void Rotate(Vector3 direction)
    {
        Quaternion worldRotation = Quaternion.LookRotation(direction, Vector3.up);

        m_CharacterController.transform.rotation = worldRotation;
    }

    public void ApplyMovement(Vector3 direction, bool sprinting, Vector3? groundNormal = null)
    {
        float movementSpeed = m_Settings.Speed;
        int inverseInt;

        //allow for deccelleration
        if (direction != Vector3.zero)
        {
            if (m_Stopped)                                                          //reset accel
                m_acceleration = 0;

            m_PreviousDirection = direction;
            m_Stopped = false;
            inverseInt = 1;
        }
        else
        {
            m_Stopped = true;
            inverseInt = -1;
        }

        //acceleration
        m_acceleration += inverseInt * (Time.deltaTime/ m_Settings.DeltaTimeToGetTopSpeed );
        m_acceleration = Mathf.Clamp(m_acceleration, 0, 1);
       
        movementSpeed *= m_acceleration;

        //Sprinting
        if (sprinting)
            movementSpeed *= m_Settings.SprintMultiplier;

        m_jumpAccel -= m_Settings.AirDrag * Time.deltaTime;
        m_jumpAccel = Mathf.Clamp(m_jumpAccel, 1, m_Settings.JumpBoostMultiplier);
        movementSpeed *= m_jumpAccel;   

        //apply movement
        if (groundNormal != null)                                                    //for slanted surfaces
        {
            
            Vector3 groundTangent = m_PreviousDirection;
            Vector3 actualGroundNormal = (Vector3)groundNormal;
            Vector3.OrthoNormalize(ref actualGroundNormal, ref groundTangent);
            m_Velocity += groundTangent.normalized * movementSpeed;

            if ((Vector3)groundNormal != Vector3.up)                                //keep player from bouncing
            {
                m_Velocity += Physics.gravity * SLOPEFORCE;
            }
        }
        else                                                                       //for air
        {
            m_PreviousDirection.Normalize();
            m_Velocity += m_PreviousDirection * movementSpeed;
        }
    }

    public void InitJump()
    {
        m_Velocity = new Vector3
            (
                m_Velocity.x,
                Mathf.Sqrt(2 * Physics.gravity.magnitude * m_Settings.JumpHeight),
                m_Velocity.z 
            );
        m_jumpAccel = m_Settings.JumpBoostMultiplier;
    }

    public void End()
    {
        m_CharacterController.Move(m_Velocity * Time.deltaTime);
    }
    
}
