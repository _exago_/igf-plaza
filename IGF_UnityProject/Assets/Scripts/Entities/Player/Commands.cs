﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public interface IButtonCommand
    {
        void Execute();
    }

    public interface IAxisCommand
    {
        void Execute(Vector2 axes);
    }


    #region IAxisCommands
    public class MoveCommand : IAxisCommand
    {
        private MovableEntity owner;
        private Camera camera;

        public MoveCommand(MovableEntity movableEntity, Camera userCamera)
        {
            owner = movableEntity;
            camera = userCamera;
        }
        void IAxisCommand.Execute(Vector2 axes)
        {
            Vector3 forwardDirection = Vector3.Scale(camera.transform.forward, new Vector3(1, 0, 1)).normalized;
            Vector3 rightDirection = Vector3.Scale(camera.transform.right, new Vector3(1, 0, 1)).normalized;


            Vector3 worldDirection = rightDirection * axes.x + forwardDirection * axes.y;
            owner.Move(worldDirection);
        }
    }

    public class RotateCommand : IAxisCommand
    {
        private MovableEntity owner;
        private Camera camera;

        public RotateCommand(MovableEntity movableEntity, Camera userCamera)
        {
            owner = movableEntity;
            camera = userCamera;
        }
        void IAxisCommand.Execute(Vector2 axes)
        {
            Vector3 forwardDirection = Vector3.Scale(camera.transform.forward, new Vector3(1, 0, 1)).normalized;
            Vector3 rightDirection = Vector3.Scale(camera.transform.right, new Vector3(1, 0, 1)).normalized;

            Vector3 worldDirection = rightDirection * axes.x + forwardDirection * axes.y;
            owner.Rotate(worldDirection);
        }
    }
    #endregion

    #region IButtonCommands
    public class JumpCommand : IButtonCommand
    {
        private PlayerBehaviour player;

        public JumpCommand(PlayerBehaviour playerCharacter)
        {
            player = playerCharacter;
        }
        void IButtonCommand.Execute()
        {
            Debug.Log("Jump");
            player.Jump();
        }
    }

    public class InteractCommand : IButtonCommand
    {
        private PlayerBehaviour player;

        public InteractCommand(PlayerBehaviour playerCharacter)
        {
            player = playerCharacter;
        }

        void IButtonCommand.Execute()
        {
            player.Interact();
        }
    }

    public class SprintCommand : IButtonCommand
    {
        private PlayerBehaviour player;

        public SprintCommand(PlayerBehaviour playerCharacter)
        {
            player = playerCharacter;
        }

        void IButtonCommand.Execute()
        {
            player.ToggleSprint();
        }
    }


    #endregion

