﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnvironmentInfo : MonoBehaviour
{
    readonly static private LayerMask groundLayerMask = LayerMask.NameToLayer("Ground");

    public static bool RaycastToGround(Vector3 pos, out RaycastHit hit, float maxDistance)                                                 
    {
        Ray rayToFloor = new Ray(pos, Vector3.down);

        return Physics.Raycast(rayToFloor, out hit, maxDistance, groundLayerMask.value);
    }
}
