﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class ModifyLightRadius : MonoBehaviour
{
    [SerializeField] private Light _pointLight = null;

    private void Update()
    {
        _pointLight.intensity += 0.2f;
        _pointLight.range += 0.2f;

    }
}
