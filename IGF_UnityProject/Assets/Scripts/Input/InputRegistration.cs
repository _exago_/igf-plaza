﻿using UnityEngine;

public abstract class InputRegistration
{
    protected string m_InputName;

    public InputRegistration(string inputName)
    {
        m_InputName = inputName;
    }

    public static InputRegistration Create(string inputName, IButtonCommand command)
    {
        return new ButtonRegistration(inputName, command);
    }

    public static InputRegistration Create(string inputName, string secondInputName, IAxisCommand command)
    {
        return new JoyStickRegistration(inputName, secondInputName, command);
    }

    public abstract void Update();
}

public class JoyStickRegistration : InputRegistration
{
    private string m_SecondInputName;
    private IAxisCommand m_Command;
    public JoyStickRegistration(string inputName, string secondInputName, IAxisCommand command) : base(inputName)
    {
        m_SecondInputName = secondInputName;
        m_Command = command;
    }

    public override void Update()
    {
        float xAxis = Input.GetAxis(m_InputName);
        float yAxis = Input.GetAxis(m_SecondInputName);

        Vector2 direction = new Vector2(xAxis, yAxis);

        m_Command.Execute(direction);
    }
}

public class ButtonRegistration : InputRegistration
{
    private IButtonCommand m_Command;
    private bool m_Toggle = false;

    public ButtonRegistration(string inputName, IButtonCommand command, bool toggle = false) : base(inputName)
    {
        m_Command = command;
        m_Toggle = toggle;
    }

    public override void Update()
    {
        if(Input.GetButtonDown(m_InputName))
            m_Command.Execute();


        if (m_Toggle)
            if (Input.GetButtonUp(m_InputName))
                m_Command.Execute();
    }
}