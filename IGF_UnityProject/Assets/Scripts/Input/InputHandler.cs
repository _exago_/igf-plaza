﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class InputHandler
{
    private List<InputRegistration> m_InputRegistrations = new List<InputRegistration>();

    public void Update()
    {
        foreach(InputRegistration registration in m_InputRegistrations)
        {
            registration.Update();
        }
    }

    public void Register(InputRegistration registration)
    {
        m_InputRegistrations.Add(registration);
    }
}