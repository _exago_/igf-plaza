﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadialMenuController : MonoBehaviour
{
    public GameObject theMenu;
    public GameObject objRed, objBlue, objGreen, objYellow;
    public Vector2 moveInput;
    public Text[] options;
    public Color normalColor, highlightedColor;
    public int selectedOption;
    //public GameObject highlightedSegment;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            theMenu.SetActive(true);
        }

        if (theMenu.activeInHierarchy)
        {
            moveInput.x = Input.mousePosition.x - (Screen.width / 2f);
            moveInput.y = Input.mousePosition.y - (Screen.height / 2f);
            moveInput.Normalize();

            if (moveInput != Vector2.zero)
            {
                float angle = Mathf.Atan2(moveInput.y, -moveInput.x) / Mathf.PI;
                angle *= 180;
                angle += 90f;

                if (angle < 0)
                {
                    angle += 360;
                }

                for (int i = 0; i < options.Length; i++)
                {
                    if (angle > i * 72 && angle < (i + 1) * 72)
                    {
                        options[i].color = highlightedColor;
                        selectedOption = i;

                        //highlightedSegment.transform.rotation = Quaternion.Euler(0,0,i * -72);
                    }
                    else
                    {
                        options[i].color = normalColor;
                    }
                }
            }

            if (Input.GetMouseButtonDown(0))
            {
                switch (selectedOption)
                {
                    case 0:
                        SwitchYellow();
                        break;
                    case 1:
                        SwitchRed();
                        break;
                    case 2:
                        SwitchAll();
                        break;
                    case 3:
                        SwitchGreen();
                        break;
                    case 4:
                        SwitchBlue();
                        break;
                }
                theMenu.SetActive(false);
            }
        }
    }

    public void SwitchRed()
    {
        objRed.SetActive(!objRed.activeInHierarchy);
    }

    public void SwitchBlue()
    {
        objBlue.SetActive(!objBlue.activeInHierarchy);
    }

    public void SwitchGreen()
    {
        objGreen.SetActive(!objGreen.activeInHierarchy);
    }

    public void SwitchYellow()
    {
        objYellow.SetActive(!objYellow.activeInHierarchy);
    }

    public void SwitchAll()
    {
        SwitchRed();
        SwitchBlue();
        SwitchGreen();
        SwitchYellow();
    }
}
